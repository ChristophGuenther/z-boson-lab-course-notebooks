# Z-Boson-Lab-Course-Notebooks

Jupyter notebooks for the Z-Boson laboratory course offered at the physics institute 3A/B at RWTH Aachen.

Open profile in RWTH Jupyter: [![](https://jupyter.pages.rwth-aachen.de/documentation/images/badge-launch-rwth-jupyter.svg)](https://jupyter.rwth-aachen.de/hub/spawn?profile=zblc)